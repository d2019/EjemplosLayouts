package dam.di.panes1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class Relative extends AppCompatActivity implements View.OnClickListener {

    private ImageView imagenFondo;
    private Button buttonHome, buttonCenter, buttonCenterCrop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.relative_layout);

        setUi();

        imagenFondo.setImageResource(R.drawable.cipfpbatoi_logo_300ppp_fonsblanc);
    }

    void setUi() {
        imagenFondo = findViewById(R.id.mainImage);
        buttonCenter = findViewById(R.id.buttonCenter);
        buttonCenterCrop = findViewById(R.id.buttonCenter);
        buttonHome = findViewById(R.id.buttonHome);

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.buttonCenter:
                imagenFondo.setScaleType(ImageView.ScaleType.CENTER);
                break;

            case R.id.buttonCenterCrop:
                imagenFondo.setScaleType(ImageView.ScaleType.CENTER_CROP);
                break;

            case R.id.buttonCenterInside:
                imagenFondo.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
                break;

            case R.id.buttonFitCenter:
                imagenFondo.setScaleType(ImageView.ScaleType.FIT_CENTER);
                break;

            case R.id.buttonFitXY:
                imagenFondo.setScaleType(ImageView.ScaleType.FIT_XY);
                break;

            case R.id.buttonMatrix:
                imagenFondo.setScaleType(ImageView.ScaleType.MATRIX);
                break;

            case R.id.buttonHome:
                startActivity(new Intent(this, MainHub.class));
                break;
        }
    }
}