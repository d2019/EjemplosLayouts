package dam.di.panes1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;

public class Login extends AppCompatActivity {

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        setUi();

        imageView.setImageResource(R.drawable.cipfpbatoi_logo_300ppp_fonsblanc);
    }

    void setUi() {
        imageView = findViewById(R.id.mainImage);
    }
}