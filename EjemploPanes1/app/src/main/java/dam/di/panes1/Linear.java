package dam.di.panes1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Linear extends AppCompatActivity implements View.OnClickListener{

    private Button buttonHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.linear_layout);

        SetUi();
    }

    private void SetUi() {
        buttonHome = findViewById(R.id.buttonHome);
    }

    @Override
    public void onClick(View v) {
        startActivity(new Intent(this, MainHub.class));
    }
}