package dam.di.panes1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainHub extends AppCompatActivity implements View.OnClickListener {

    private Button buttonConstraint, buttonRelative, buttonLinear, buttonTable, buttonGrid, buttonForm, buttonDescarga;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_hub);

        SetUi();
    }

    private void SetUi() {
    buttonConstraint = findViewById(R.id.buttonMatrix);
    buttonRelative = findViewById(R.id.relativeButton);
    buttonDescarga = findViewById(R.id.descargaButton);
    buttonLinear = findViewById(R.id.linearButton);
    buttonTable = findViewById(R.id.tableButton);
    buttonGrid = findViewById(R.id.gridButton);
    buttonForm = findViewById(R.id.formButton);
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.constraintButton:
                startActivity(new Intent(this, Login.class));
                break;

            case R.id.relativeButton:
                startActivity(new Intent(this, Relative.class));
                break;

            case R.id.linearButton:
                startActivity(new Intent(this, Linear.class));
                break;

            case R.id.tableButton:
                startActivity(new Intent(this, Table.class));
                break;

            case R.id.gridButton:
                startActivity(new Intent(this, Grid.class));
                break;

            case R.id.formButton:
                startActivity(new Intent(this, Form.class));
                break;

            case R.id.descargaButton:
                startActivity(new Intent(this, Descarga.class));
                break;
        }
    }
}